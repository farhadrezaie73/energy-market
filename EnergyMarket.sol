pragma solidity ^0.5.1;

import "./DateTimeLibrary.sol";

// Owner Register
contract Owned {
    address owner;
    uint8 producerID = 1;
    uint8 consumerID = 1;
    
    constructor() public {
      owner = msg.sender; 
    }
  
  modifier only_owner {
    require(msg.sender == owner);
    _;
  }
}

// Consumer Contract Register
contract ConsumerRegistry is Owned {

  mapping(address => uint8) public consumers;

  modifier only_registered_consumers {
    require(consumers[msg.sender] > 0);
    _;
  }
  
  function register_consumer(address _consumer) only_owner external {
    consumers[_consumer] = consumerID;
    consumerID +=1;
  }
}

// Producer Contract Register
contract ProducerRegistry is Owned {

    mapping(address => uint8) public producers;
    modifier only_registered_producers {
       require(producers[msg.sender] > 0);
    _;
    }
    
    function register_producer(address _producer) only_owner external {
        producers[_producer] = producerID;
        producerID +=1;
      }
}

contract EnergyMarket is Owned, ConsumerRegistry, ProducerRegistry {
    
    enum Resource {Invalid, Solar, Wind, Bio, Other }
    Resource public resource;
    Resource constant defaultResource = Resource.Invalid;
    
    enum PowerCheck { Invalid, Valid }
    PowerCheck public powercheck;
    
    enum BudgetCheck { Invalid, Valid }
    BudgetCheck public budgetcheck;
    
    enum UserCheck { Producer, Consumer }
    UserCheck public usercheck;
    
    enum ZoneCheck {Invalid, first,second,third}
    ZoneCheck public zonecheck;

    uint64 constant kWh = 1;
    uint8 constant PRICE_REFERENCE = 100;
    uint min;
    uint max;
    uint idx_producer;
    uint idx_consumer;
    uint idx_d_consumer;
    uint year;
    uint month;
    uint day;
    uint hour;
    uint minute;
    uint second;
    
    struct bid_producer {
        address producer;
        uint32 zone;
        uint64 price;
        uint64 power;
        uint64 max_power_transition;
        uint64 resourceKind;
        uint8 producer_index;
        uint256 month;
        uint256 day;
        uint256 hour;
        uint256 minute;
        uint256 second;
        uint256 block_index;
    
    }

    struct demand_consumer {
        address consumer;
        uint32 zone;
        uint64 price;
        uint64 power;
        uint64 resourceKind;
        uint8 d_consumer_index;
        uint256 month;
        uint256 day;
        uint256 hour;
        uint256 minute;
        uint256 second;
        uint256 block_index;
    }
    
    struct ask_consumer{
        address producer;
        address consumer;
        uint32 zone;
        uint64 price;
        uint64 power;
        uint64 power_transition;
        uint64 budget;
        uint8 consumer_buy_index;
        uint256 month;
        uint256 day;
        uint256 hour;
        uint256 minute;
        uint256 second;
        uint256 block_index;
    }
    
    struct ask_producer {
        address producer;
        address consumer;
        uint32 zone;
        uint64 price;
        uint64 power;
        uint64 power_transition;
        uint8 producer_sell_index;
        uint256 month;
        uint256 day;
        uint256 hour;
        uint256 minute;
        uint256 second;
        uint256 block_index;
    }

    bid_producer[] private bidsProducer;
    demand_consumer[] private demandConsumer;
    ask_consumer[] private asksConsumer;
    ask_producer[] private asksProducer;
    
    mapping(address => uint) private bidsIndex;
    
    mapping(uint8 => uint) private asksIndex;
    
     function UserProducer() public {
        usercheck = UserCheck.Producer;
    }
      
    function UserConsumer() public {
        usercheck = UserCheck.Consumer;
    }
    
    function SetResourceSolar() public {
        resource = Resource.Solar;
        min = 5;
        max = 6;
    }
      
    function SetResourceWind() public {
        resource = Resource.Wind;
        min = 6;
        max = 8;
    }
      
    function SetResourceBio() public {
        resource = Resource.Bio;
        min = 8;
        max = 12;
    }
      
    function SetResourceOther() public {
        resource = Resource.Other;
        min = 1;
        max = 2;
    }
    
    function SetZone(uint8 number) public {
        if (number == 1) {
            zonecheck = ZoneCheck.first;
        }
        if (number == 2){
            zonecheck = ZoneCheck.second;
        }
        if (number == 3){
            zonecheck = ZoneCheck.third;
        }
    }
    
    function ValidPower() public {
        powercheck = PowerCheck.Valid;
    }
    
    function InvalidPower() public {
        powercheck = PowerCheck.Invalid;
    }
    
    function ValidBudget() public {
        budgetcheck = BudgetCheck.Valid;
    }
    
    function InvalidBudget() public {
        budgetcheck = BudgetCheck.Invalid;
    }
    
    uint idx = bidsIndex[msg.sender];
    uint idx_o_producer = bidsIndex[msg.sender];
    uint idx_d_consumer_sell = bidsIndex[msg.sender];
    uint idx_sell_producer = bidsIndex[msg.sender];
    uint idx_change_producer = bidsIndex[msg.sender];
    uint idx_change_consumer = bidsIndex[msg.sender];
    
    // energy offer by producer
    
    function offer_energy(address _producer, uint32 _producer_zone, uint64 _price, uint64 _power,uint64 _power_transition, uint64 _resourceKind, uint8 _producer_index) only_registered_producers external {
        idx_producer = asksIndex[_producer_index];
        require(_power >= kWh);
        require(PRICE_REFERENCE*min <= _price && _price <= PRICE_REFERENCE*max);
        
        uint timestamp = block.timestamp;
        month = DateTimeLibrary.getMonth(timestamp);
        day = DateTimeLibrary.getDay(timestamp);
        hour = DateTimeLibrary.getHour(timestamp);
        minute = DateTimeLibrary.getMinute(timestamp);
        second = DateTimeLibrary.getSecond(timestamp);
        
        if ((uint(powercheck) == 1) && (uint(usercheck) == 0) && (uint(resource) == _resourceKind)){
                asksIndex[_producer_index] = bidsProducer.length;
                idx = bidsProducer.length;
                bidsIndex[msg.sender] = idx;
                bidsProducer.push(bid_producer({
                producer: _producer,
                zone: _producer_zone,
                price: _price,
                power: _power,
                max_power_transition: _power_transition,
                resourceKind: _resourceKind,
                producer_index: _producer_index,
                month: month, 
                day: day, 
                hour: hour,
                minute: minute,
                second: second,
                block_index:block.number
                }));
                idx_producer += 1;
        }else{
            revert();
        }
    }
    
    // energy demand by consumer
    
    function demand_energy(address _consumer, uint32 _consumer_zone, uint64 _price, uint64 _power, uint64 _resourceKind, uint8 _d_consumer_index) only_registered_consumers external {
        idx_d_consumer = asksIndex[_d_consumer_index];
        require(_power >= kWh);
        require(PRICE_REFERENCE*min <= _price && _price <= PRICE_REFERENCE*max);
        
        uint timestamp = block.timestamp;
        month = DateTimeLibrary.getMonth(timestamp);
        day = DateTimeLibrary.getDay(timestamp);
        hour = DateTimeLibrary.getHour(timestamp);
        minute = DateTimeLibrary.getMinute(timestamp);
        second = DateTimeLibrary.getSecond(timestamp);
        
        if ((uint(budgetcheck) == 1) && (uint(usercheck) == 1)){
                asksIndex[_d_consumer_index] = demandConsumer.length;
                idx = demandConsumer.length;
                bidsIndex[msg.sender] = idx;
                demandConsumer.push(demand_consumer({
                consumer: _consumer,
                zone: _consumer_zone,
                price: _price,
                power: _power,
                resourceKind: _resourceKind,
                d_consumer_index: _d_consumer_index,
                month: month, 
                day: day, 
                hour: hour,
                minute: minute,
                second:second,
                block_index:block.number
                }));
                idx_d_consumer += 1;
        }else{
            revert();
        }
    }
    
    // get energy offer information by producer offer ID

    function GetBidByProducer(uint8 producer_index) external view returns(address producer, uint32 zone, uint64 price, uint64 power,uint64 max_power_transition, uint64 resourceKind, uint256 month, uint256 day, uint256 hour, uint256 minute, uint256 second, uint256 block_index) {
        uint idx_producer = asksIndex[producer_index];
        require(bidsProducer[idx_producer].producer_index == producer_index);
        return (bidsProducer[idx_producer].producer, bidsProducer[idx_producer].zone, bidsProducer[idx_producer].price, bidsProducer[idx_producer].power, bidsProducer[idx_producer].max_power_transition, bidsProducer[idx_producer].resourceKind, bidsProducer[idx_producer].month, bidsProducer[idx_producer].day, bidsProducer[idx_producer].hour, bidsProducer[idx_producer].minute, bidsProducer[idx_producer].second, bidsProducer[idx_producer].block_index);
    }
    
    // get energy demand information by consumer demand ID
    
    function GetDemandByConsumer(uint8 _d_consumer_index) external view returns(address consumer, uint32 zone, uint64 price, uint64 power, uint64 resourceKind, uint256 month, uint256 day, uint256 hour, uint256 minute, uint256 second, uint256 block_index) {
        uint idx_d_consumer = asksIndex[_d_consumer_index];
        require(demandConsumer[idx_d_consumer].d_consumer_index == _d_consumer_index);
        return (demandConsumer[idx_d_consumer].consumer, demandConsumer[idx_d_consumer].zone, demandConsumer[idx_d_consumer].price, demandConsumer[idx_d_consumer].power, demandConsumer[idx_d_consumer].resourceKind, demandConsumer[idx_d_consumer].month, demandConsumer[idx_d_consumer].day, demandConsumer[idx_d_consumer].hour, demandConsumer[idx_d_consumer].minute, demandConsumer[idx_d_consumer].second, demandConsumer[idx_d_consumer].block_index);
    }
    
    // get energy buy information by buy transaction ID
    
    function getBuyEnergy(uint8 _idx_buy) external view returns(address producer, address consumer, uint32 zone, uint64 price, uint64 power, uint64 power_transition, uint256 month, uint256 day, uint256 hour, uint256 minute, uint256 second) {
        uint idx_d_buy = asksIndex[_idx_buy];
        require(asksConsumer[idx_d_buy].consumer_buy_index == _idx_buy);
        return (asksConsumer[idx_d_buy].producer, asksConsumer[idx_d_buy].consumer, asksConsumer[idx_d_buy].zone, asksConsumer[idx_d_buy].price, asksConsumer[idx_d_buy].power, asksConsumer[idx_d_buy].power_transition, asksConsumer[idx_d_buy].month, asksConsumer[idx_d_buy].day, asksConsumer[idx_d_buy].hour, asksConsumer[idx_d_buy].minute, asksConsumer[idx_d_buy].second);

    }
    
    // get energy sell information by sell transaction ID
    
    function getSellEnergy(uint8 _idx_sell) external view returns(address producer, address consumer, uint32 zone, uint64 price, uint64 power, uint64 power_transition, uint256 month, uint256 day, uint256 hour, uint256 minute, uint256 second) {
        uint idx_d_sell = asksIndex[_idx_sell];
        require(asksProducer[idx_d_sell].producer_sell_index == _idx_sell);
        return (asksProducer[idx_d_sell].producer, asksProducer[idx_d_sell].consumer, asksProducer[idx_d_sell].zone, asksProducer[idx_d_sell].price, asksProducer[idx_d_sell].power, asksProducer[idx_d_sell].power_transition, asksProducer[idx_d_sell].month, asksProducer[idx_d_sell].day, asksProducer[idx_d_sell].hour, asksProducer[idx_d_sell].minute, asksProducer[idx_d_sell].second);
    }
    
    // buy energy - energy buy main function is in this function
    
    function buy_energy(address _producer, address _consumer, uint32 _consumer_zone, uint64 _price, uint64 _power, uint64 _power_transition, uint64 _budget, uint8 _consumer_index, uint8 _producer_index) only_registered_consumers external {
        buy_energy_core(_producer, _consumer, _consumer_zone, _price, _power, _power_transition, _budget, _consumer_index, _producer_index);
    }
    
    // sell energy - energy sell main function is in this function
    
    function sell_energy(address _producer, address _consumer, uint32 _producer_zone, uint64 _price, uint64 _power, uint64 _power_transition, uint8 _consumer_sell_index, uint8 _producer_sell_index) only_registered_producers external {
        sell_energy_core(_producer, _consumer, _producer_zone, _price, _power, _power_transition, _consumer_sell_index, _producer_sell_index);
    }
    
    // main function for energy sell function

    function sell_energy_core(address _producer, address _consumer, uint32 _producer_zone, uint64 _price, uint64 _power, uint64 _power_transition, uint8 _consumer_sell_index, uint8  _producer_sell_index) internal {
        idx_sell_producer = asksIndex[_producer_sell_index];
        idx_d_consumer_sell = asksIndex[_consumer_sell_index];
        
        uint timestamp = block.timestamp;
        month = DateTimeLibrary.getMonth(timestamp);
        day = DateTimeLibrary.getDay(timestamp);
        hour = DateTimeLibrary.getHour(timestamp);
        minute = DateTimeLibrary.getMinute(timestamp);
        second = DateTimeLibrary.getSecond(timestamp);
        
        if ( (demandConsumer[idx_d_consumer_sell].consumer == _consumer)){
            require(_power >= demandConsumer[idx_d_consumer_sell].power);
            if((uint(usercheck) == 0) && (uint(powercheck) == 1)){
                require(demandConsumer[idx_d_consumer_sell].power >= _power);
                require(demandConsumer[idx_d_consumer_sell].price*_power == _price);
            }
            idx = asksProducer.length;
            asksIndex[_producer_sell_index] = idx;
            asksProducer.push(ask_producer({
            producer: _producer,
            consumer: _consumer,
            zone: _producer_zone,
            price: _price,
            power: _power,
            power_transition: _power_transition,
            producer_sell_index: _producer_sell_index,
            month: month, 
            day: day, 
            hour: hour,
            minute: minute,
            second: second,
            block_index:block.number
            }));
            idx_sell_producer +=1;
            
            demandConsumer[idx_d_consumer_sell].power = demandConsumer[idx_d_consumer_sell].power - _power;
        }else{
            revert();
        }
    }
    
    // main function for energy buy function
    
    function buy_energy_core(address _producer, address _consumer, uint32 _consumer_zone, uint64 _price, uint64 _power, uint64 _power_transition, uint64 _budget, uint8 _consumer_index, uint8  _producer_index) internal {
        idx_o_producer = asksIndex[_producer_index];
        idx_consumer = asksIndex[_consumer_index];
        
        uint timestamp = block.timestamp;
        month = DateTimeLibrary.getMonth(timestamp);
        day = DateTimeLibrary.getDay(timestamp);
        hour = DateTimeLibrary.getHour(timestamp);
        minute = DateTimeLibrary.getMinute(timestamp);
        second = DateTimeLibrary.getSecond(timestamp);

        if ( (bidsProducer[idx_o_producer].producer == _producer)){
            require(_budget >= _price);
            if((uint(usercheck) == 1) && (uint(budgetcheck) == 1)){
                require(bidsProducer[idx_o_producer].power >= _power);
                require(bidsProducer[idx_o_producer].price*_power == _price);
                require(_power_transition <= bidsProducer[idx_o_producer].max_power_transition);
            }
        
            idx = asksConsumer.length;
            asksIndex[_consumer_index] = idx;
            asksConsumer.push(ask_consumer({
            producer: _producer,
            consumer: _consumer,
            zone: _consumer_zone,
            price: _price,
            power: _power,
            power_transition: _power_transition,
            budget: _budget,
            consumer_buy_index: _consumer_index,
            month: month, 
            day: day, 
            hour: hour,
            minute: minute,
            second: second,
            block_index:block.number
            }));
            idx_consumer +=1;
            
            bidsProducer[idx_o_producer].power = bidsProducer[idx_o_producer].power - _power;
            
        }else{
            revert();
        }
    }
    
    // change price or power for energy offer
    
    function changeOffer(uint8 _producer_index, uint64 _power, uint64 _price) only_registered_producers external{
        uint idx_change_offer = asksIndex[_producer_index];
        
        uint timestamp = block.timestamp;
        month = DateTimeLibrary.getMonth(timestamp);
        day = DateTimeLibrary.getDay(timestamp);
        hour = DateTimeLibrary.getHour(timestamp);
        minute = DateTimeLibrary.getMinute(timestamp);
        second = DateTimeLibrary.getSecond(timestamp);

        if ( (bidsProducer[idx_change_offer].producer == msg.sender)){
            bidsProducer[idx_change_offer].power = _power;
            bidsProducer[idx_change_offer].price = _price;
        }else{
            revert();
        }
    }
    
    // change price or power for energy demand
    
    function changeDemand(uint8 _consumer_index, uint64 _power, uint64 _price) only_registered_consumers external{
        uint idx_change_demand = asksIndex[_consumer_index];
        
        uint timestamp = block.timestamp;
        month = DateTimeLibrary.getMonth(timestamp);
        day = DateTimeLibrary.getDay(timestamp);
        hour = DateTimeLibrary.getHour(timestamp);
        minute = DateTimeLibrary.getMinute(timestamp);
        second = DateTimeLibrary.getSecond(timestamp);

        if ( (demandConsumer[idx_change_demand].consumer == msg.sender)){
            demandConsumer[idx_change_demand].power = _power;
            demandConsumer[idx_change_demand].price = _price;
        }else{
            revert();
        }
    }
}